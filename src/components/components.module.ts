import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ReportsHeaderComponent } from './reports-header/reports-header';
@NgModule({
	declarations: [ReportsHeaderComponent],
	imports: [
		IonicPageModule.forChild(ReportsHeaderComponent),
		TranslateModule.forChild(),
	],
	exports: [ReportsHeaderComponent]
})
export class ComponentsModule {}
