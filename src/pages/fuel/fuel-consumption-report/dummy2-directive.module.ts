import { NgModule } from "@angular/core";
import { OnCreate2 } from "./dummy2-directive";

@NgModule({
  declarations: [OnCreate2],
  exports: [OnCreate2]
})
export class OnCreate2Module {

}
